# Android Contact Book

Before all steps below, ensure, that '/path/to/sdk/emulator' and '/path/to/sdk/platform-tools' are in your PATH!

## Installation
 - Clone the repo
 - ...In project directory
 - npm install
 - add 'local.properties' file in 'Project Dir/android/local.properties' with the following content:
        sdk.dir=/path/to/sdk

## Run
 - ...In project directory
 - npm run android
 - or react-native run-android

## Build
 - ...In project directory
 - cd android
 - ./gradlew assembleRelease
 - you can find apk file in 'Project Dir/android/app/build/outputs/apk/release/app-release.apk'
