import { ToastAndroid } from "react-native";

export function showError(err) {
    ToastAndroid.showWithGravity(err.message, ToastAndroid.LONG, ToastAndroid.CENTER);
}
