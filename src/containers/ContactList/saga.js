import { all, takeEvery, put, call } from "redux-saga/effects";
import { getAllContactsPromisified, addContactPromisified, deleteContactPromisified } from "./helper";
import { setContacts, loadContacts } from "./actions";
import { LOAD_CONTACTS, ADD_CONTACT, DELETE_CONTACT } from "./actionTypes";
import { PermissionsAndroid } from "react-native";
import { showError } from "../../error.helper";

function* loadContactsGen() {
    try {
        yield call(PermissionsAndroid.requestMultiple, [
            PermissionsAndroid.PERMISSIONS.READ_CONTACTS,
            PermissionsAndroid.PERMISSIONS.WRITE_CONTACTS
        ]);

        const contacts = yield call(getAllContactsPromisified);
        yield put(setContacts(contacts));
    } catch (err) {
        showError(err);
    }
}

function* onLoadContacts() {
    yield takeEvery(LOAD_CONTACTS, loadContactsGen);
}

function* addContactGen({ firstName, lastName, telephone, hasImage, imagePath }) {
    try {
        yield call(addContactPromisified, {
            firstName,
            lastName,
            telephone,
            hasImage,
            imagePath
        });

        yield put(loadContacts());
    } catch (err) {
        showError(err);
    }
}

function* onAddContact() {
    yield takeEvery(ADD_CONTACT, addContactGen);
}

function* deleteContactGen({ id }) {
    try {
        yield call(deleteContactPromisified, id);
        yield put(loadContacts());
    } catch (err) {
        showError(err);
    }
}

function* onDeleteContact() {
    yield takeEvery(DELETE_CONTACT, deleteContactGen);
}

export default function* () {
    yield all([
        onLoadContacts(),
        onAddContact(),
        onDeleteContact()
    ]);
}
