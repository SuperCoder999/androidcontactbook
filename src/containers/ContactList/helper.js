import Contacts from "react-native-contacts";

export function getAllContactsPromisified() {
    return new Promise((resolve, reject) => {
        Contacts.getAll((err, contacts) => {
            if (err) {
                return reject(err);
            }

            return resolve(contacts);
        });
    });
}

export function addContactPromisified({ firstName, lastName, telephone, hasImage, imagePath }) {
    return new Promise((resolve, reject) => {
        Contacts.addContact(
            {
                givenName: firstName,
                familyName: lastName,
                phoneNumbers: [{
                    label: "mobile",
                    number: telephone
                }],
                hasThumbnail: hasImage,
                thumbnailPath: imagePath || ""
            },
            err => {
                if (err) {
                    return reject(err);
                }

                return resolve();
            }
        );
    });
}

export function deleteContactPromisified(id) {
    return new Promise((resolve, reject) => {
        Contacts.deleteContact(
            { recordID: id },
            err => {
                if (err) {
                    return reject(err);
                }

                return resolve();
            }
        );
    });
}
