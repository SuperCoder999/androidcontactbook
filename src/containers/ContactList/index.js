import React, { useEffect, useState } from "react";
import { FlatList, StyleSheet, Button } from "react-native";
import { connect } from "react-redux";
import { loadContacts } from "./actions";
import Contact from "../../components/Contact";
import { setExpandedContact } from "./actions";
import SearchInput from "../../components/SearchInput";

function ContactList({ contacts, expandedContact, loadContacts, expandContact, navigation }) {
    const [contactsList, setContactsList] = useState(contacts);
    const [canUpdate, setCanUpdate] = useState(false);

    useEffect(() => {
        if (expandedContact) {
            navigation.push("Expanded");
        }

        if ((contacts ? (contactsList.length !== contacts.length) : false) && canUpdate) {
            setContactsList(contacts);
            setCanUpdate(false);
        }
    });

    useEffect(() => {
        const unsubscribe = navigation.addListener("focus", () => setCanUpdate(true));
        return unsubscribe;
    });

    if (!contacts) {
        loadContacts();
        return null;
    }

    if (!contactsList) {
        setContactsList(contacts);
    }

    const renderContact = ({ item: contact }) =>
        <Contact contact={contact} expand={expandContact} />;

    const filterArray = text => {
        const regex = new RegExp(text.replace(/[!@#$%^&*()+=\-[\]\\';,./{}|":<>?~_]/g, "\\$&"), "ig");

        setContactsList(
            contacts.filter(contact => {
                const nameMatch = regex.test(contact.displayName);
                let phoneMatch = false;

                if (contact.phoneNumbers[0]) {
                    phoneMatch = regex.test(contact.phoneNumbers[0].number);
                }

                return nameMatch || phoneMatch;
            })
        );
    }

    return (
        <>
            <SearchInput onChange={filterArray} />
            <FlatList
                style={style.container}
                data={contactsList}
                keyExtractor={contact => contact.recordID}
                renderItem={renderContact}
            />
            <Button title="Add contact" onPress={() => navigation.push("Add")} />
        </>
    );
}

const style = StyleSheet.create({
    container: {
        flex: 1
    }
});

const mapStateToProps = state => ({
    contacts: state.contacts.contacts,
    expandedContact: state.contacts.expanded
});

const mapDispatchToProps = dispatch => ({
    loadContacts: () => dispatch(loadContacts()),
    expandContact: contact => dispatch(setExpandedContact(contact))
});

export default connect(mapStateToProps, mapDispatchToProps)(ContactList);
