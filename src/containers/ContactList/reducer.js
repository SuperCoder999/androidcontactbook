import { SET_CONTACTS, SET_EXPANDED_CONTACT } from "./actionTypes";

const initalState = {
    contacts: null,
    expanded: null
}

export default (state = initalState, action) => {
    switch (action.type) {
        case SET_CONTACTS: {
            return {
                ...state,
                contacts: action.contacts
            };
        }
        case SET_EXPANDED_CONTACT: {
            return {
                ...state,
                expanded: action.contact
            };
        }
        default: {
            return state;
        }
    }
};
