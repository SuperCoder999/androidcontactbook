import { LOAD_CONTACTS, SET_CONTACTS, SET_EXPANDED_CONTACT, ADD_CONTACT, DELETE_CONTACT } from "./actionTypes";

export const loadContacts = () => ({
    type: LOAD_CONTACTS
});

export const setContacts = contacts => ({
    type: SET_CONTACTS,
    contacts
});

export const setExpandedContact = contact => ({
    type: SET_EXPANDED_CONTACT,
    contact
});

export const addContact = ({ firstName, lastName, telephone, hasImage, imagePath }) => ({
    type: ADD_CONTACT,
    firstName,
    lastName,
    telephone,
    hasImage,
    imagePath
});

export const deleteContact = id => ({
    type: DELETE_CONTACT,
    id
});
