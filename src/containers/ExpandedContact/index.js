import React, { useEffect } from "react";
import { View, Image, Text, StyleSheet, Button } from "react-native";
import { connect } from "react-redux";
import { setExpandedContact, deleteContact } from "../ContactList/actions";
import { DEFAULT_AVATAR } from "../../config";
import call from "react-native-phone-call";

function ExpandedContact({ contact, setNull, deleteContact, navigation }) {
    useEffect(() => {
        if (!contact) {
            navigation.pop();
        }
    });

    const del = () => {
        setNull();
        deleteContact(contact.recordID);
    };

    if (contact) {
        const phoneNumber = contact.phoneNumbers[0];
        const name = contact.displayName;

        const image = {
            uri: contact.hasThumbnail
                ? contact.thumbnailPath
                : DEFAULT_AVATAR
        };

        return (
            <View style={style.contactBlock}>
                <Image source={image} style={style.contactImage} />
                <Text style={[style.contactBlockItem, style.contactBlockText]}>{name}</Text>
                <Text style={[style.contactBlockItem, style.contactBlockText]}>{phoneNumber ? phoneNumber.number : "No number"}</Text>
                <View style={[style.toolbar, style.contactBlockItem]}>
                    {phoneNumber
                        ? <Button onPress={() => call({ number: phoneNumber.number })} title="Call" />
                        : <View />}
                    <Button onPress={del} title="Delete" color="red" />
                    <Button onPress={setNull} title="Back" />
                </View>
            </View>
        );
    }

    return null;
}

const style = StyleSheet.create({
    contactBlock: {
        width: "100%",
        height: "100%",
        justifyContent: "center",
        alignItems: "center"
    },
    contactBlockItem: {
        marginTop: 50
    },
    contactImage: {
        borderRadius: 1000,
        width: 100,
        height: 100
    },
    toolbar: {
        flexDirection: "row",
        justifyContent: "space-around",
        width: "100%"
    },
    contactBlockText: {
        width: "70%",
        borderBottomColor: "#DDD",
        color: "#333",
        borderBottomWidth: 1,
        paddingBottom: 10,
        textAlign: "center"
    }
});

const mapStateToProps = state => ({
    contact: state.contacts.expanded
});

const mapDispatchToProps = dispatch => ({
    setNull: () => dispatch(setExpandedContact(null)),
    deleteContact: id => dispatch(deleteContact(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(ExpandedContact);
