import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import ContactList from "../ContactList";
import ExpandedContact from "../ExpandedContact";
import AddContactForm from "../AddContactForm";

const Stack = createStackNavigator();

function ContactsNavigator() {
    return (
        <Stack.Navigator>
            <Stack.Screen
                name="List"
                component={ContactList}
                options={{
                    header: () => null
                }}
            />
            <Stack.Screen
                name="Expanded"
                component={ExpandedContact}
                options={{
                    header: () => null
                }}
            />
            <Stack.Screen
                name="Add"
                component={AddContactForm}
                options={{
                    header: () => null
                }}
            />
        </Stack.Navigator>
    );
}

export default ContactsNavigator;
