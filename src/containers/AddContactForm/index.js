import React, { useState } from "react";
import { View, TextInput, Text, Button, StyleSheet } from "react-native";
import { addContact } from "../ContactList/actions";
import { connect } from "react-redux";
import ImagePicker from "react-native-image-picker";
import { showError } from "../../error.helper";
import FA_Icon from "react-native-vector-icons/FontAwesome";
import { TWO_MEGABYTES } from "../../config";

function AddContactForm({ add, navigation }) {
    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [telephone, setTelephone] = useState("");
    const [imgPath, setImgPath] = useState(null);

    const isDisabled = !(firstName.trim() && telephone.trim()) || !(/^((\+\d{1,3})|\d)\d{9}$/.test(telephone.trim()));

    const submit = () => {
        if (isDisabled) return;

        add({
            firstName,
            lastName: lastName || "",
            telephone,
            hasImage: Boolean(imgPath),
            imagePath: imgPath
        });

        navigation.pop();
    };

    const selectImage = () => {
        ImagePicker.showImagePicker(
            { title: "Select contact photo" },
            response => {
                if (response.error) {
                    return showError({ message: response.error });
                }

                if (response.fileSize >= TWO_MEGABYTES) {
                    return showError({ message: "File is too big. Max allowed size is 2 MB" });
                }

                setImgPath(response.path);
            }
        )
    };

    return (
        <View>
            <Text style={style.header}>Add contact</Text>
            <TextInput
                style={style.input}
                placeholder="First name"
                onChangeText={text => setFirstName(text.toString())}
            />
            <TextInput
                style={style.input}
                placeholder="Last name"
                onChangeText={text => setLastName(text.toString())}
            />
            <TextInput
                style={style.input}
                placeholder="Telephone"
                value={telephone}
                onChangeText={text => {
                    if (/^((\+\d{0,3})|\d)?\d{0,9}$/.test(text)) {
                        setTelephone(text.toString());
                    }
                }}
                textContentType="telephoneNumber"
            />
            {imgPath
                ? (
                    <Text style={style.greenText}>
                        <FA_Icon name="check" />
                        Photo selected
                    </Text>
                )
                : <></>}
            <Button
                title="Select photo"
                onPress={selectImage}
                color="green"
            />
            <Button
                style={style.button}
                disabled={isDisabled}
                onPress={submit}
                title="Add contact"
            />
            <Button
                title="Cancel"
                color="black"
                onPress={() => navigation.pop()}
            />
        </View>
    );
}

const style = StyleSheet.create({
    greenText: {
        color: "green",
        textAlign: "center",
        marginBottom: 10,
        marginTop: 10
    },
    header: {
        color: "green",
        fontSize: 24,
        textAlign: "center",
        marginTop: 10,
        marginBottom: 10
    },
    button: {
        marginTop: 10
    },
    input: {
        paddingLeft: 30,
        paddingRight: 10
    }
});

const mapDispatchToProps = dispatch => ({
    add: settings => dispatch(addContact(settings))
});

export default connect(() => ({}), mapDispatchToProps)(AddContactForm);
