import React from "react";
import { TextInput, View, StyleSheet } from "react-native";
import FA_Icon from "react-native-vector-icons/FontAwesome";

function SearchInput({ onChange }) {
    return (
        <View style={style.searchBlock}>
            <FA_Icon style={style.searchBlockIcon} name="search" />
            <TextInput
                style={style.searchBlockInput}
                onChangeText={onChange}
                placeholder="Search..."
            />
        </View>
    );
}

const style = StyleSheet.create({
    searchBlock: {
        height: 50,
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#fff",
    },
    searchBlockIcon: {
        paddingLeft: 20,
        paddingRight: 10,
        paddingTop: 10,
        paddingBottom: 10,
        fontSize: 24
    },
    searchBlockInput: {
        flex: 1,
        paddingTop: 10,
        paddingRight: 10,
        paddingBottom: 10,
        paddingLeft: 10,
        backgroundColor: "#fff",
        color: "#424242"
    }
});

export default SearchInput;
