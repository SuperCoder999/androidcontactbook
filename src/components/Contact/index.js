import React from "react";
import { View, Image, Text, StyleSheet, TouchableOpacity } from "react-native";
import { DEFAULT_AVATAR } from "../../config";

function Contact({ contact, expand }) {
    const phoneNumber = contact.phoneNumbers[0];
    const name = contact.displayName;

    const image = {
        uri: contact.hasThumbnail
            ? contact.thumbnailPath
            : DEFAULT_AVATAR
    };

    return (
        <TouchableOpacity onPress={() => expand({ ...contact })}>
            <View style={style.contactBlock}>
                <Image source={image} style={style.contactImage} />
                <View style={style.contactTexts}>
                    <Text>{name}</Text>
                    <Text>{phoneNumber ? phoneNumber.number : "No number"}</Text>
                </View>
            </View>
        </TouchableOpacity>
    );
}

const style = StyleSheet.create({
    contactBlock: {
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "row",
        marginTop: 10
    },
    contactImage: {
        width: 50,
        height: 50,
        borderRadius: 50,
        marginLeft: 20
    },
    contactTexts: {
        width: "75%",
        flex: 1,
        justifyContent: "center",
        marginLeft: 20
    }
});

export default Contact;
