import { combineReducers } from "redux";
import contacts from "../containers/ContactList/reducer";

export default combineReducers({ contacts });
