import { all } from "redux-saga/effects";
import contacts from "../containers/ContactList/saga";

export default function* () {
    yield all([
        contacts()
    ]);
}
