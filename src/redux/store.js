import { createStore, applyMiddleware } from "redux";
import createSagaMiddleware from "redux-saga";
import reducer from "./reducer";
import saga from "./saga";

const sagaMiddleware = createSagaMiddleware();

const store = createStore(
    reducer,
    {
        contacts: {
            contacts: null,
            expanded: null
        }
    },
    applyMiddleware(sagaMiddleware)
);

sagaMiddleware.run(saga);

export default store;
