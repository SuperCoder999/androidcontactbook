import React from "react";
import Contacts from "./src/containers/ContactsNavigator";
import { Provider as ReduxProvider } from "react-redux";
import store from "./src/redux/store";
import { NavigationContainer } from "@react-navigation/native";

function App() {
    return (
        <ReduxProvider store={store}>
            <NavigationContainer>
                <Contacts />
            </NavigationContainer>
        </ReduxProvider> 
    );
}

export default App;
